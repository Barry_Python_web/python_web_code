from flask import Flask, jsonify
app = Flask(__name__)
def get_weather(city):
  # 根据城市名称查询天气
  # 返回查询结果
  return {'city': city, 'weather': 'sunny'}
@app.route('/weather/<city>')
def weather(city):
  # 调用get_weather函数获取天气情况
  result = get_weather(city)
  # 将查询结果以json格式返回
  return jsonify(result)
if __name__ == '__main__':
  app.run()